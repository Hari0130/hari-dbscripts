

CREATE TABLE USERS (
    user_id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(50)  NOT NULL,
    last_name VARCHAR(50)  NOT NULL,
    dob DATE NOT NULL,
    user_login_id VARCHAR(100)  NOT NULL,
    password VARCHAR(50)  NOT NULL,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
    usertype ENUM('BUSINESS','CUSTOMER','EMPLOYEE') NOT NULL,
    PRIMARY KEY (user_id),
    UNIQUE KEY(user_login_id)
);

CREATE TABLE USER_ROLE
(
    user_role_id INT NOT NULL AUTO_INCREMENT,
    user_role_code VARCHAR(10) NOT NULL,
    role_description VARCHAR(255) ,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
	PRIMARY KEY (user_role_id),
	UNIQUE KEY(user_role_code)

);
