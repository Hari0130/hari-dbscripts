
CREATE TABLE CATEGORY
(
    category_id INT NOT NULL AUTO_INCREMENT,
    category_name VARCHAR(255) NOT NULL,
    category_code VARCHAR(20) NOT NULL,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
    PRIMARY KEY (category_id) ,
    UNIQUE KEY (category_code)
);

CREATE TABLE SUB_CATEGORIES
(
    sub_category_id INT NOT NULL AUTO_INCREMENT,
    sub_category_name VARCHAR(255) NOT NULL,
    category_id INTEGER NOT NULL,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
    PRIMARY KEY (sub_category_id) ,
    FOREIGN KEY (category_id) REFERENCES CATEGORY(category_id)
);
